#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <gccore.h>
#include <wiiuse/wpad.h>
#include <ogc/machine/processor.h>
#include "mload.h"

extern void *_binary_______fakemote_build_FAKEMOTE_app_start;
extern void *_binary_______fakemote_build_FAKEMOTE_app_size;

static const void *test_module_elf_start = &_binary_______fakemote_build_FAKEMOTE_app_start;
//static const u32 test_module_elf_size = (u32)&_binary_______fakemote_build_FAKEMOTE_app_size;

static int run = 1;
static int mload_thid = -1;

static void button_pressed()
{
	run = 0;
}

int __IOS_LoadStartupIOS(void)
{
	/* Load IOS before C runtime to have MLOAD before CONF_Init() is called */
	IOS_ReloadIOS(249);

	data_elf info;
	mload_init();
	//mload_set_log_mode(DEBUG_GECKO);
	mload_set_log_mode(DEBUG_BUFFER);
	mload_elf(test_module_elf_start, &info);
	mload_thid = mload_run_thread(info.start, info.stack, info.size_stack, info.prio);

	return 0;
}

int main(int argc, char **argv)
{
	int ret;
	void *xfb;
	GXRModeObj *rmode;

	SYS_SetResetCallback(button_pressed);
	SYS_SetPowerCallback(button_pressed);

	VIDEO_Init();

	rmode = VIDEO_GetPreferredMode(NULL);
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	console_init(xfb, 0, 0, rmode->fbWidth, rmode->xfbHeight-2, rmode->fbWidth * VI_DISPLAY_PIX_SZ);
	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(xfb);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if (rmode->viTVMode&VI_NON_INTERLACE)
		VIDEO_WaitVSync();

	//usleep(500 * 1000);
	WPAD_Init();

	printf("\x1b[2;0H");
	printf("Entering main loop\n");

	#define LOG_SIZE (4096)
	char *log = memalign(32, LOG_SIZE);
	memset(log, 0, LOG_SIZE);

	while (run) {
		WPAD_ScanPads();
		u32 pressed0 = WPAD_ButtonsHeld(0);
		u32 pressed1 = WPAD_ButtonsHeld(1);
		if ((pressed0 | pressed1) & WPAD_BUTTON_HOME)
			run = 0;
		if (pressed0)
			printf("Pressed[0]: 0x%08x\n", pressed0);
		if (pressed1)
			printf("Pressed[1]: 0x%08x\n", pressed1);

		u32 exp_type;
		WPAD_Probe(0, &exp_type);
		WPADData *data = WPAD_Data(0);
		if (exp_type == WPAD_EXP_NUNCHUK) {
			printf("x: %03d, y: %03d\n", data->exp.nunchuk.js.pos.x, data->exp.nunchuk.js.pos.y);
		}

#if 0
		/* Log ringbuffer consumer */
		u32 cnt_to_end;
		do {
			u32 head = read32((uintptr_t)rb_head_uc);
			u32 tail = read32((uintptr_t)rb_tail_uc);

			cnt_to_end = CIRC_CNT_TO_END(head, tail, rb_size);

			if (cnt_to_end != 0) {
				static char buf[4096+1];

				memset(buf, 0, sizeof(buf));
				//DCInvalidateRange(rb_data + tail, cnt_to_end);
				DCInvalidateRange(rb_data, rb_size);
				memcpy(buf, rb_data + tail, cnt_to_end);
				//buf[cnt_to_end + 1] = '\0';

				//printf("head: 0x%x, tail: 0x%x  ->  cnt_to_end: 0x%x\n", head, tail, cnt_to_end);
				printf("%s\n", buf);

				tail = (tail + cnt_to_end) & (rb_size - 1);
				write32((uintptr_t)rb_tail_uc, tail);
			}
		} while (cnt_to_end > 0);
#else
		memset(log, 0, LOG_SIZE);
		DCFlushRange(log, LOG_SIZE);
		ret = mload_get_log_buffer_and_empty(log, LOG_SIZE);
		if (ret > 0) {
			DCInvalidateRange(log, LOG_SIZE);
			printf("%s", log);
		}
		usleep(1000);
#endif

		VIDEO_WaitVSync();
	}

	printf("\n\nExiting...\n");

	free(log);

	ret = mload_stop_thread(mload_thid);
	printf("mload_stop_thread(): %d\n", ret);

	ret = mload_close();
	printf("mload_close(): %d\n", ret);

	/*free(rb_head);
	free(rb_tail);
	free(rb_data);*/

	return 0;
}
